# storage/nvdimm/creat_namespace_simultaneously_on_every_cpu

Storage: nvdimm creat namespace simultaneously on every cpu from BZ1492049

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
